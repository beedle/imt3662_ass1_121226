package net.sbstensby.ass1_121226;

import org.w3c.dom.Document;

/**
 * Created by SverreB on 15.09.2014.
 * This is only here to give RequestXML a function to run in SubredditActivity.
 */
public interface AsyncResponse {
    public void documentResponse(Document result);
}
