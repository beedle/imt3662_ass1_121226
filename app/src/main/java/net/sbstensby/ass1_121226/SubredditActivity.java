package net.sbstensby.ass1_121226;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;


public class SubredditActivity extends Activity implements AsyncResponse {
    TextView textBox;
    RequestXML requestXML = new RequestXML();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subreddit);

        textBox = (TextView)findViewById(R.id.out_text_view);

        // Request xml from a subreddit.
        Intent intent = getIntent();
        requestXML.delegate = this; // Make it run documentResponse here when done.
        requestXML.execute("http://www.reddit.com/r/"+intent.getStringExtra("subreddit")+"/hot.xml");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.text, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void documentResponse(Document output) {
        NodeList items = output.getElementsByTagName("item");
        String str = "";
        for (int i = 0; items.getLength() > i; i++) {
            str += items.item(i).getChildNodes().item(0).getTextContent() + "\n";
        }
        if (str == "") {
            str = "Unable to find that subreddit. If you need a subreddit to test, try \"androiddev\".";
        }
        textBox.setText(str);
    }
}
