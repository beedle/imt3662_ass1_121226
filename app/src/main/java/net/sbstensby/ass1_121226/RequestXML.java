package net.sbstensby.ass1_121226;

import android.os.AsyncTask;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by SverreB on 15.09.2014.
 * Special thanks to http://stackoverflow.com/a/3506039
 */

class RequestXML extends AsyncTask<String, String, Document> {
    public AsyncResponse delegate = null;

    @Override
    protected Document doInBackground(String... uri) {
        HttpClient httpclient = new DefaultHttpClient();
        HttpResponse response;
        Document doc = null;
        try {
            response = httpclient.execute(new HttpGet(uri[0]));
            StatusLine statusLine = response.getStatusLine();

            if(statusLine.getStatusCode() == HttpStatus.SC_OK){
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = factory.newDocumentBuilder();
                doc = builder.parse(response.getEntity().getContent());
            }
            else {
                // Close the connection
                response.getEntity().getContent().close();
                throw new IOException(statusLine.getReasonPhrase());
            }
        }
        catch (ClientProtocolException e) {
            // TODO Handle Exception.
        }
        catch (IOException e) {
            // TODO Handle Exception.
        }
        catch (ParserConfigurationException e) {
            // TODO Handle Exception.
        }
        catch (SAXException e) {
            // TODO Handle Exception.
        }
        return doc;
    }

    @Override
    protected void onPostExecute(Document result) {
        super.onPostExecute(result);
        delegate.documentResponse(result);
    }
}