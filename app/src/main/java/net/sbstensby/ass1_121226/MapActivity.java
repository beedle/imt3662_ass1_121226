package net.sbstensby.ass1_121226;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;


public class MapActivity extends Activity implements AdapterView.OnItemSelectedListener, LocationListener {
    private GoogleMap                   mMap;               // The map that goes on screen
    private LocationManager             locationManager;    // The location manager
    private Spinner                     mapMode;            // The dropdown
    private ArrayAdapter<CharSequence>  adapter;            // Adapter between strings.xml and mapMode
    private int[]                       mapTypes;           // Types of map in same order as mapMode

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        // Set up locationManager and mMap
        setUpLocationManagerIfNeeded();
        setUpMapIfNeeded();

        // Initialize mapTypes. Warning, if the order is changed, change order of map_modes_array in strings.xml
        mapTypes = new int[] {  GoogleMap.MAP_TYPE_NORMAL,
                                GoogleMap.MAP_TYPE_HYBRID,
                                GoogleMap.MAP_TYPE_SATELLITE,
                                GoogleMap.MAP_TYPE_TERRAIN};

        // Set up mapMode dropdown.
        mapMode = (Spinner) findViewById(R.id.map_mode);
        adapter = ArrayAdapter.createFromResource(this, R.array.map_modes_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mapMode.setAdapter(adapter);
        mapMode.setOnItemSelectedListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLocationChanged(Location location) {
        // When location changes, animate camera to new location.
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLng(latLng);
        mMap.animateCamera(cameraUpdate);
        locationManager.removeUpdates(this);
    }

    @Override
    public void onProviderEnabled(String provider) {
        // Do nothing
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // Do nothing
    }

    @Override
    public void onProviderDisabled(String provider) {
        // Do nothing
    }

    private void setUpMapIfNeeded() {
        // Check if map is defined
        if (mMap == null) {
            // Try to get the map
            mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
            // If it worked
            if (mMap != null) {
                // Show my location
                mMap.setMyLocationEnabled(true);
                // Move view to last known location.
                onLocationChanged(locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER));
            }

        }
    }

    private void setUpLocationManagerIfNeeded() {
        // Check if it needs defining
        if (locationManager == null) {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

            if (locationManager != null) {
                // Get updates
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5, 1000, this);
            }
        }
    }

    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        // Set the maptype
        mMap.setMapType(mapTypes[pos]);
    }

    public void onNothingSelected(AdapterView<?> parent) {
        // Do nothing
    }
}
