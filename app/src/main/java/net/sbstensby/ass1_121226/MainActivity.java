package net.sbstensby.ass1_121226;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;


public class MainActivity extends Activity implements View.OnClickListener {
    Button mapButton;
    Button SubredditButton;
    EditText SubredditEditText;
    public static final String PREFS_NAME = "Ass1prefs";
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mapButton  = (Button) findViewById(R.id.map_button);
        SubredditButton = (Button) findViewById(R.id.subreddit_button);
        SubredditEditText = (EditText) findViewById(R.id.subreddit_name);

        mapButton.setOnClickListener(this);
        SubredditButton.setOnClickListener(this);


        sharedPreferences  = getSharedPreferences(PREFS_NAME, 0);
        String subreddit = sharedPreferences.getString("subreddit", "HarryPotter");
        SubredditEditText.setText(subreddit);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

    }

    @Override
    protected void onStop() {
        super.onStop();

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("subreddit", SubredditEditText.getText().toString());
        editor.apply();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.map_button: {
                startActivity(new Intent(this, MapActivity.class));
                break;
            }
            case R.id.subreddit_button: {
                Intent intent = new Intent(this, SubredditActivity.class);
                intent.putExtra("subreddit", SubredditEditText.getText().toString());
                startActivity(intent);
                break;
            }
        }

    }
}
